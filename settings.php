<?php
// ------------------------------------------------------------------
// Вешаем все блоки, поля и опции на хук admin_init
// ------------------------------------------------------------------
//
add_action( 'admin_init', 'eg_settings_api_init' );
function eg_settings_api_init() {
	add_settings_section(
		'travelpayouts_setting_section', // секция
		'Настройки для TravelPayouts',
		'travelpayouts_setting_section_callback_function',
		'general' // страница
	);

	add_settings_field(
		'travelpayouts_token',
		'Token для доступа к API',
		'travelpayouts_setting_callback_token',
		'general', // страница
		'travelpayouts_setting_section' // секция
	);

	add_settings_field(
		'travelpayouts_cache',
		'Время хранения кеша в секундах',
		'travelpayouts_setting_callback_cache',
		'general', // страница
		'travelpayouts_setting_section' // секция
	);

	register_setting( 'general', 'travelpayouts_token' );
	register_setting( 'general', 'travelpayouts_cache' );
}

function travelpayouts_setting_section_callback_function() {
	echo '<p>Настроки API TravelPayouts</p>';
}

function travelpayouts_setting_callback_cache() {
	echo '<input 
		name="travelpayouts_cache"  
		type="text" 
		value="' . get_option( 'travelpayouts_cache' , 3600) . '" 
		class="travelpayouts_cache"
	 />';
}

function travelpayouts_setting_callback_token() {
	echo '<input 
		name="travelpayouts_token"  
		type="text" 
		value="' . get_option( 'travelpayouts_token' ) . '" 
		class="travelpayouts_token"
	 />';
}