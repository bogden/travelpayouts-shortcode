<?php
/**
 * Plugin Name: Tourprice
 * Plugin URI:
 * Description:
 * Version: 0.0.1
 * Author: Bogdan Panazdyr
 *
 * @package tourprice-wordpress-plugin
 */

include 'settings.php';
add_filter( 'the_title', 'filter_function_travelpayouts', 10, 2 );
add_filter( 'document_title_parts', 'filter_function_travelpayouts', 100, 1 );

function filter_function_travelpayouts( $title, $id = null ) {
	global $post;
	$is   = get_post_meta( $post->ID, 'show_price_in_title', true );
	$from = get_post_meta( $post->ID, 'title_price_from', true );
	$to   = get_post_meta( $post->ID, 'title_price_to', true );
	if ( $is && $from && $to ) {
		$result = do_shortcode( '[travelpayouts origin=' . $from . ' destination=' . $to . ' one_way=true template=\' от {value^} руб.\']' );
	}
	if ( is_array( $title ) ) {
		$title[] = strip_tags( $result );
	} elseif ( $post->ID === $id ) {
		$title =  $title . $result;
	}

	return $title;
}

function plural_form( $number, $after ) {
	$cases = array( 2, 0, 1, 1, 1, 2 );

	return number_format( $number, 0, ',',
			' ' ) . ' ' . $after[ ( $number % 100 > 4 && $number % 100 < 20 ) ? 2 : $cases[ min( $number % 10, 5 ) ] ];
}

function travelpayouts_shortcode( $atts ) {
	$token      = get_option( 'travelpayouts_token', false );
	$cache_time = get_option( 'travelpayouts_cache', 3600 );
	if ( ! $token ) {
		return 'token is require';
	}
	$atts           = shortcode_atts( [
		'token'         => $token,
		'origin'        => false,
		'destination'   => false,
		'currency'      => 'rub',
		'limit'         => 1,
		'one_way'       => false,
		'method'        => 'latest',
		'version'       => 'v2',
		'template'      => '{value*}',
		'show-in-title' => false,
		'depart_date'   => null,
		'return_date'   => null,
		'page'          => null,
		'flexibility '  => null,
		'trip_class'    => null,
		'actual'        => null,
		'default'       => ''
	], $atts );
	$cache_key      = 'travelpayouts_' . md5( implode( ',', [
			(string) $atts['origin'],
			(string) $atts['destination'],
			(string) $atts['currency'],
			(string) $atts['method'],
			(string) $atts['limit'],
			(string) $atts['depart_date'],
			(string) $atts['return_date'],
			date( 'd.m.Y' ),
		] ) );
	$cache_key_meta = 'travelpayouts_' . md5( implode( ',', [ $cache_key, $atts['template'] ] ) );
	$meta_cache     = get_post_meta( get_the_ID(), $cache_key_meta, true );
	if ( $meta_cache ) {
		return $meta_cache;
	}

	if ( ! $atts['origin'] || ! $atts['destination'] ) {
		return 'Не указан пункт отправления или назначения';
	}
	if ( ! $atts['origin'] || ! $atts['destination'] ) {
		return 'Не указан пункт отправления или назначения';
	}

	$parameters = '?';
	switch ( $atts['method'] ) {
		case 'latest':
			$url = 'http://api.travelpayouts.com/v2/prices/latest';
			break;
		case 'cheap':
			$url = 'http://api.travelpayouts.com/v1/prices/cheap';
			break;
		case 'direct':
			$url = 'http://api.travelpayouts.com/v1/prices/direct';
			break;
		default:
			$url = 'http://api.travelpayouts.com/' . $atts['version'] . $atts['method'];
			break;
	}
	foreach ( $atts as $key => $value ) {
		if ( $value && $key !== 'method' && $key !== 'template' && $key !== 'show-in-title' && $key !== 'default' ) {
			$parameters .= $key . '=' . $value . '&';
		}
	}
	$cache = wp_cache_get( $cache_key, 'default' );
	$data  = null;
	if ( ! $cache || ! $cache->cache_key || time() < ( $cache->cache_time + $cache_time ) ) {
		$request = wp_remote_get( $url . $parameters );
		$data    = json_decode( $request['body'] );
		if ( $data->success ) {
			$data->cache_time = time();
			$data->cache_key  = $cache_key;
			wp_cache_set( $cache_key, $data, 'default', get_option( 'travelpayouts_cache_time', 18600 ) );
		} else {
			return 'API Error';
		}
	} else {
		$data = $cache;
	}
	$items = $atts['method'] === 'latest' ? $data->data : (array) $data->data->{$atts['destination']};
	if ( $items && is_array( $items ) ) {
		if ( count( $items ) > 1 && ! $atts['show-in-title'] ) {
			$result = '<ul class="travelpayouts-list">';
			foreach ( $items as $item ) {
				$result .= '<li>' . travelpayouts_text_template( $atts['template'], $item, $atts ) . '</li>';
			}
			$result .= '</ul>';

			return $result;
		} elseif ( count( $items ) ) {
			$result = travelpayouts_text_template( $atts['template'], $items[0], $atts );
			update_post_meta( get_the_ID(), $cache_key_meta, $result );

			return $result;
		}
	} else {
		return $atts['default'];
	}

	return $atts['default'];
}

function travelpayouts_text_template( $template = '', $data, $atts = [] ) {
	$result = str_replace( '{value*}',
		plural_form( $data->value, [ 'Рубль', 'Рубля', 'Рублей' ] ), $template );
	$result = str_replace( '{value^}',
		number_format( $data->value, 0, ',', ' ' ), $result );
	$result = str_replace( '{price*}',
		plural_form( $data->price, [ 'Рубль', 'Рубля', 'Рублей' ] ), $result );

	$result = str_replace( '{distance*}', $data->distance . 'км.', $result );
	preg_match_all( '/(\{(\w*)\})/usi', $template, $matches );

	$replace_data = [];
	foreach ( $matches[2] as $value ) {
		if ( preg_match( '/[0-9]{2,4}\-[0-9]{1,2}\-[0-9]{1,2}/usi', $data->{$value} ) ) {
			$data->{$value} = date( 'd.m.Y', strtotime( $data->{$value} ) );
		}
		$replace_data[] = $data->{$value};
	}
	$result = str_replace( $matches[1], $replace_data, $result );

	return '<span data-info="' . $atts['method'] . '">' . $result . '</span>';
}

add_shortcode( 'travelpayouts', 'travelpayouts_shortcode' );


add_action( 'add_meta_boxes', 'travelpayouts_extra_fields', 1 );

function travelpayouts_extra_fields() {
	add_meta_box( 'extra_fields', 'Цена тура в заголовке', 'extra_fields_box_func', ['post', 'slideshow', 'portfolio'], 'normal', 'high' );
}

function extra_fields_box_func( $post ) {
	?>
    <p>Видимость поста: <?php $mark_v = get_post_meta( $post->ID, 'show_price_in_title', true ); ?>
        <label><input type="radio" name="extra[show_price_in_title]" value="" <?php checked( $mark_v, '' ); ?> />
            Не показывать</label>
        <label><input type="radio" name="extra[show_price_in_title]" value="1" <?php checked( $mark_v, '1' ); ?> />
            Показывать стоимость в заголовке</label>
    </p>
    <p>
        <label>
            <input type="text" name="extra[title_price_from]"
                   value="<?php echo get_post_meta( $post->ID, 'title_price_from', true ); ?>"/>
            Пункт отправления
        </label>
    </p>
    <p>
        <label>
            <input type="text" name="extra[title_price_to]"
                   value="<?php echo get_post_meta( $post->ID, 'title_price_to', true ); ?>"/>
            Пункт назначения
        </label>
    </p>
    <input type="hidden" name="extra_fields_nonce" value="<?php echo wp_create_nonce( __FILE__ ); ?>"/>
	<?php
}

add_action( 'save_post', 'my_extra_fields_update', 0 );

## Сохраняем данные, при сохранении поста
function my_extra_fields_update( $post_id ) {
	// базовая проверка
	if (
		empty( $_POST['extra'] )
		|| ! wp_verify_nonce( $_POST['extra_fields_nonce'], __FILE__ )
		|| wp_is_post_autosave( $post_id )
		|| wp_is_post_revision( $post_id )
	) {
		return false;
	}

	$_POST['extra'] = array_map( 'sanitize_text_field', $_POST['extra'] ); // чистим все данные от пробелов по краям
	foreach ( $_POST['extra'] as $key => $value ) {
		if ( empty( $value ) ) {
			delete_post_meta( $post_id, $key ); // удаляем поле если значение пустое
			continue;
		}
		update_post_meta( $post_id, $key, $value ); // add_post_meta() работает автоматически
	}

	return $post_id;
}